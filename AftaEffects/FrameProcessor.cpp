#include "FrameProcessor.h"
#include "MyForm.h"

using namespace System;

FrameProcessor::FrameProcessor()
{
	_canny = false;
	_sobel = false;
	_grayscale = false;
	_negative = false;
	_h_mirror = false;
	_v_mirror = false;
	_recording = false;
	_brightness_amount = 0;
	_contrast_amount = 1;
	_filter_amount = 0;
	_orientation = FrameOrientation::NORMAL;
}

cv::Mat FrameProcessor::ProcessFrame(cv::Mat frame)
{
	// scale
	if (_scale_factor != 1)
		cv::resize(frame, frame, cv::Size(), _scale_factor, _scale_factor);

	// rotate
	switch (_orientation)
	{
		case FrameOrientation::NORMAL:
			break;
		case FrameOrientation::ROT_90_R:
			cv::flip(frame.t(), frame, 1);
			break;
		case FrameOrientation::ROT_180:
			cv::flip(frame, frame, -1);
			break;
		case FrameOrientation::ROT_90_L:
			cv::flip(frame.t(), frame, 0);
			break;
		default:
			break;
	}

	// mirrors
	if (_h_mirror)
		cv::flip(frame, frame, 1);

	if (_v_mirror)
		cv::flip(frame, frame, 0);

	// grayscale
	if (_grayscale)
	{
		cvtColor(frame, frame, cv::COLOR_BGR2GRAY);
		cvtColor(frame, frame, cv::COLOR_GRAY2BGR);
	}

	// filters
	if (_canny)
	{
		cv::extractChannel(frame, frame, 0); // must be single channel

		cv::blur(frame, frame, cv::Size(3, 3));
		cv::Canny(frame, frame, _filter_amount, _filter_amount * 3);

		cvtColor(frame, frame, cv::COLOR_GRAY2BGR);
	}
	else if (_sobel)
	{
		cv::blur(frame, frame, cv::Size(3, 3));

		int ksize = 1;
		if (_filter_amount >= 7)
			ksize = 7;
		else if (_filter_amount >= 5)
			ksize = 5;
		else if (_filter_amount >= 3)
			ksize = 3;

		cv::Sobel(frame, frame, -1, 1, 1, ksize);
	}
	else if (_filter_amount != 0)
	{
		cv::GaussianBlur(frame, frame, cv::Size(0, 0), _filter_amount);
	}

	// brightness / constrast
	if (_brightness_amount != 0 || _contrast_amount != 1)
		frame.convertTo(frame, -1, _contrast_amount, _brightness_amount);

	// negative
	if (_negative)
		frame.convertTo(frame, -1, -1, 255);

	return frame;
}

FrameProcessor::FrameOrientation FrameProcessor::getOrientation()
{
	return _orientation;
}

Boolean FrameProcessor::isRecording()
{
	return _recording;
}

Void FrameProcessor::setBrightnessAmount(Int32 val)
{
	_brightness_amount = val;
}

Void FrameProcessor::setContrastAmount(Int32 val)
{
	_contrast_amount = val;
}

Void FrameProcessor::setFilterAmount(Int32 val)
{
	_filter_amount = val;
}

Void FrameProcessor::setCanny(Boolean val)
{
	_canny = val;
}

Void FrameProcessor::setSobel(Boolean val)
{
	_sobel = val;
}

Void FrameProcessor::setGrayscale(Boolean val)
{
	_grayscale = val;
}

Void FrameProcessor::setNegative(Boolean val)
{
	_negative = val;
}

Void FrameProcessor::setHorizontalMirror(Boolean val)
{
	_h_mirror = val;
}

Void FrameProcessor::setVerticalMirror(Boolean val)
{
	_v_mirror = val;
}

Void FrameProcessor::setRecording(Boolean val)
{
	_recording = val;
}

Void FrameProcessor::setScaleFactor(Double val)
{
	_scale_factor = val;
}

Void FrameProcessor::setOrientation(FrameOrientation val)
{
	_orientation = val;
}

Void FrameProcessor::HalfSize()
{
	_scale_factor /= 2;
}

Void FrameProcessor::DoubleSize()
{
	_scale_factor *= 2;
}

Void FrameProcessor::RotateLeft()
{
	Int32 orientation = static_cast<Int32>(_orientation);
	orientation = (orientation - 1 + 4) % 4;
	_orientation = static_cast<FrameOrientation>(orientation);
}

Void FrameProcessor::RotateRight()
{
	Int32 orientation = static_cast<Int32>(_orientation);
	orientation = (orientation + 1) % 4;
	_orientation = static_cast<FrameOrientation>(orientation);
}