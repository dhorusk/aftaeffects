#pragma once

#pragma unmanaged
#include <opencv2/opencv.hpp>
#pragma managed

#include "FrameProcessor.h"
#include <msclr\marshal_cppstd.h>

template<typename T>
inline T clamp(const T x, const T min, const T max)
{
	return x < min ? min :
		   x > max ? max :
		   x;
}

namespace AftaEffects {
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace msclr::interop;
	
	public ref class MyForm : public Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			
			cap = new cv::VideoCapture(0);

			if (!cap->isOpened())
			{
				MessageBox::Show(L"Error:\nNo camera found!");
				exit(-1);
			}

			frame_timer->Enabled = true;
			fp = (gcnew FrameProcessor());
			fp->setCanny(canny_chkbox->Checked);
			fp->setSobel(sobel_chkbox->Checked);
			fp->setGrayscale(grayscale_chkbox->Checked);
			fp->setNegative(negative_chkbox->Checked);
			fp->setHorizontalMirror(h_mirror_chkbox->Checked);
			fp->setVerticalMirror(v_mirror_chkbox->Checked);
			fp->setRecording(false);
			fp->setScaleFactor(1);
			fp->setBrightnessAmount(0);
			fp->setContrastAmount(1);
			fp->setFilterAmount(0);
			fp->setOrientation(FrameProcessor::FrameOrientation::NORMAL);

			out = new cv::VideoWriter();
		}

	protected:
		~MyForm()
		{
			if (components)
			{
				delete components;
			}

			cap->release();
			out->release();
		}

	private: System::Windows::Forms::Timer^ frame_timer;
	private: System::Windows::Forms::TrackBar^ brightness_slider;
	private: System::Windows::Forms::TrackBar^ contrast_slider;
	private: System::Windows::Forms::TrackBar^ blur_slider;
	private: System::Windows::Forms::Label^ brightness_label;
	private: System::Windows::Forms::Label^ contrast_label;
	private: System::Windows::Forms::Label^  filter_label;
	private: System::Windows::Forms::GroupBox^ img_adj_groupbox;
	private: System::Windows::Forms::GroupBox^ resize_rotate_groupbox;
	private: System::Windows::Forms::Button^ resize_double_btn;
	private: System::Windows::Forms::Button^ resize_half_btn;
	private: System::Windows::Forms::Button^ rotate_left_btn;
	private: System::Windows::Forms::Button^ rotate_right_btn;
	private: System::Windows::Forms::Button^ record_stop_btn;
	private: System::Windows::Forms::Button^ record_start_btn;
	private: System::Windows::Forms::CheckBox^ v_mirror_chkbox;
	private: System::Windows::Forms::CheckBox^ h_mirror_chkbox;
	private: System::Windows::Forms::CheckBox^ negative_chkbox;
	private: System::Windows::Forms::CheckBox^ grayscale_chkbox;
	private: System::Windows::Forms::CheckBox^ sobel_chkbox;
	private: System::Windows::Forms::CheckBox^ canny_chkbox;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::TextBox^  blur_val;
	private: System::Windows::Forms::TextBox^  contrast_val;
	private: System::Windows::Forms::TextBox^  brightness_val;
	private: System::ComponentModel::IContainer^ components;

	private:
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(MyForm::typeid));
			this->frame_timer = (gcnew System::Windows::Forms::Timer(this->components));
			this->brightness_slider = (gcnew System::Windows::Forms::TrackBar());
			this->contrast_slider = (gcnew System::Windows::Forms::TrackBar());
			this->blur_slider = (gcnew System::Windows::Forms::TrackBar());
			this->brightness_label = (gcnew System::Windows::Forms::Label());
			this->contrast_label = (gcnew System::Windows::Forms::Label());
			this->filter_label = (gcnew System::Windows::Forms::Label());
			this->img_adj_groupbox = (gcnew System::Windows::Forms::GroupBox());
			this->blur_val = (gcnew System::Windows::Forms::TextBox());
			this->contrast_val = (gcnew System::Windows::Forms::TextBox());
			this->brightness_val = (gcnew System::Windows::Forms::TextBox());
			this->v_mirror_chkbox = (gcnew System::Windows::Forms::CheckBox());
			this->h_mirror_chkbox = (gcnew System::Windows::Forms::CheckBox());
			this->negative_chkbox = (gcnew System::Windows::Forms::CheckBox());
			this->grayscale_chkbox = (gcnew System::Windows::Forms::CheckBox());
			this->sobel_chkbox = (gcnew System::Windows::Forms::CheckBox());
			this->canny_chkbox = (gcnew System::Windows::Forms::CheckBox());
			this->resize_rotate_groupbox = (gcnew System::Windows::Forms::GroupBox());
			this->resize_double_btn = (gcnew System::Windows::Forms::Button());
			this->resize_half_btn = (gcnew System::Windows::Forms::Button());
			this->rotate_left_btn = (gcnew System::Windows::Forms::Button());
			this->rotate_right_btn = (gcnew System::Windows::Forms::Button());
			this->record_stop_btn = (gcnew System::Windows::Forms::Button());
			this->record_start_btn = (gcnew System::Windows::Forms::Button());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->brightness_slider))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->contrast_slider))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->blur_slider))->BeginInit();
			this->img_adj_groupbox->SuspendLayout();
			this->resize_rotate_groupbox->SuspendLayout();
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->SuspendLayout();
			// 
			// frame_timer
			// 
			this->frame_timer->Tick += gcnew System::EventHandler(this, &MyForm::frame_timer_Tick);
			// 
			// brightness_slider
			// 
			this->brightness_slider->Location = System::Drawing::Point(76, 19);
			this->brightness_slider->Maximum = 255;
			this->brightness_slider->Minimum = -255;
			this->brightness_slider->Name = L"brightness_slider";
			this->brightness_slider->Size = System::Drawing::Size(156, 45);
			this->brightness_slider->TabIndex = 2;
			this->brightness_slider->TickFrequency = 51;
			this->brightness_slider->ValueChanged += gcnew System::EventHandler(this, &MyForm::brightness_slider_ValueChanged);
			// 
			// contrast_slider
			// 
			this->contrast_slider->Location = System::Drawing::Point(76, 70);
			this->contrast_slider->Maximum = 100;
			this->contrast_slider->Minimum = 1;
			this->contrast_slider->Name = L"contrast_slider";
			this->contrast_slider->Size = System::Drawing::Size(156, 45);
			this->contrast_slider->TabIndex = 3;
			this->contrast_slider->TickFrequency = 10;
			this->contrast_slider->Value = 1;
			this->contrast_slider->ValueChanged += gcnew System::EventHandler(this, &MyForm::contrast_slider_ValueChanged);
			// 
			// blur_slider
			// 
			this->blur_slider->Location = System::Drawing::Point(76, 121);
			this->blur_slider->Name = L"blur_slider";
			this->blur_slider->Size = System::Drawing::Size(156, 45);
			this->blur_slider->TabIndex = 4;
			this->blur_slider->ValueChanged += gcnew System::EventHandler(this, &MyForm::blur_slider_ValueChanged);
			// 
			// brightness_label
			// 
			this->brightness_label->AutoSize = true;
			this->brightness_label->Location = System::Drawing::Point(6, 19);
			this->brightness_label->Name = L"brightness_label";
			this->brightness_label->Size = System::Drawing::Size(59, 13);
			this->brightness_label->TabIndex = 5;
			this->brightness_label->Text = L"Brightness:";
			// 
			// contrast_label
			// 
			this->contrast_label->AutoSize = true;
			this->contrast_label->Location = System::Drawing::Point(7, 70);
			this->contrast_label->Name = L"contrast_label";
			this->contrast_label->Size = System::Drawing::Size(49, 13);
			this->contrast_label->TabIndex = 6;
			this->contrast_label->Text = L"Contrast:";
			// 
			// filter_label
			// 
			this->filter_label->AutoSize = true;
			this->filter_label->Location = System::Drawing::Point(7, 121);
			this->filter_label->Name = L"filter_label";
			this->filter_label->Size = System::Drawing::Size(53, 13);
			this->filter_label->TabIndex = 7;
			this->filter_label->Text = L"Filter size:";
			// 
			// img_adj_groupbox
			// 
			this->img_adj_groupbox->Controls->Add(this->blur_val);
			this->img_adj_groupbox->Controls->Add(this->contrast_val);
			this->img_adj_groupbox->Controls->Add(this->brightness_val);
			this->img_adj_groupbox->Controls->Add(this->v_mirror_chkbox);
			this->img_adj_groupbox->Controls->Add(this->h_mirror_chkbox);
			this->img_adj_groupbox->Controls->Add(this->negative_chkbox);
			this->img_adj_groupbox->Controls->Add(this->grayscale_chkbox);
			this->img_adj_groupbox->Controls->Add(this->sobel_chkbox);
			this->img_adj_groupbox->Controls->Add(this->canny_chkbox);
			this->img_adj_groupbox->Controls->Add(this->brightness_slider);
			this->img_adj_groupbox->Controls->Add(this->filter_label);
			this->img_adj_groupbox->Controls->Add(this->contrast_slider);
			this->img_adj_groupbox->Controls->Add(this->contrast_label);
			this->img_adj_groupbox->Controls->Add(this->blur_slider);
			this->img_adj_groupbox->Controls->Add(this->brightness_label);
			this->img_adj_groupbox->Location = System::Drawing::Point(12, 8);
			this->img_adj_groupbox->Name = L"img_adj_groupbox";
			this->img_adj_groupbox->Size = System::Drawing::Size(287, 221);
			this->img_adj_groupbox->TabIndex = 8;
			this->img_adj_groupbox->TabStop = false;
			this->img_adj_groupbox->Text = L"Image adjustments:";
			// 
			// blur_val
			// 
			this->blur_val->Location = System::Drawing::Point(238, 121);
			this->blur_val->Name = L"blur_val";
			this->blur_val->Size = System::Drawing::Size(38, 20);
			this->blur_val->TabIndex = 16;
			this->blur_val->Text = L"0";
			this->blur_val->TextChanged += gcnew System::EventHandler(this, &MyForm::blur_val_TextChanged);
			// 
			// contrast_val
			// 
			this->contrast_val->Location = System::Drawing::Point(239, 70);
			this->contrast_val->Name = L"contrast_val";
			this->contrast_val->Size = System::Drawing::Size(38, 20);
			this->contrast_val->TabIndex = 15;
			this->contrast_val->Text = L"1";
			this->contrast_val->TextChanged += gcnew System::EventHandler(this, &MyForm::contrast_val_TextChanged);
			// 
			// brightness_val
			// 
			this->brightness_val->Location = System::Drawing::Point(239, 19);
			this->brightness_val->Name = L"brightness_val";
			this->brightness_val->Size = System::Drawing::Size(38, 20);
			this->brightness_val->TabIndex = 14;
			this->brightness_val->Text = L"0";
			this->brightness_val->TextChanged += gcnew System::EventHandler(this, &MyForm::brightness_val_TextChanged);
			// 
			// v_mirror_chkbox
			// 
			this->v_mirror_chkbox->AutoSize = true;
			this->v_mirror_chkbox->Location = System::Drawing::Point(175, 195);
			this->v_mirror_chkbox->Name = L"v_mirror_chkbox";
			this->v_mirror_chkbox->Size = System::Drawing::Size(90, 17);
			this->v_mirror_chkbox->TabIndex = 13;
			this->v_mirror_chkbox->Text = L"Vertical Mirror";
			this->v_mirror_chkbox->UseVisualStyleBackColor = true;
			this->v_mirror_chkbox->CheckedChanged += gcnew System::EventHandler(this, &MyForm::v_mirror_chkbox_CheckedChanged);
			// 
			// h_mirror_chkbox
			// 
			this->h_mirror_chkbox->AutoSize = true;
			this->h_mirror_chkbox->Location = System::Drawing::Point(175, 172);
			this->h_mirror_chkbox->Name = L"h_mirror_chkbox";
			this->h_mirror_chkbox->Size = System::Drawing::Size(102, 17);
			this->h_mirror_chkbox->TabIndex = 12;
			this->h_mirror_chkbox->Text = L"Horizontal Mirror";
			this->h_mirror_chkbox->UseVisualStyleBackColor = true;
			this->h_mirror_chkbox->CheckedChanged += gcnew System::EventHandler(this, &MyForm::h_mirror_chkbox_CheckedChanged);
			// 
			// negative_chkbox
			// 
			this->negative_chkbox->AutoSize = true;
			this->negative_chkbox->Location = System::Drawing::Point(83, 172);
			this->negative_chkbox->Name = L"negative_chkbox";
			this->negative_chkbox->Size = System::Drawing::Size(69, 17);
			this->negative_chkbox->TabIndex = 11;
			this->negative_chkbox->Text = L"Negative";
			this->negative_chkbox->UseVisualStyleBackColor = true;
			this->negative_chkbox->CheckedChanged += gcnew System::EventHandler(this, &MyForm::negative_chkbox_CheckedChanged);
			// 
			// grayscale_chkbox
			// 
			this->grayscale_chkbox->AutoSize = true;
			this->grayscale_chkbox->Location = System::Drawing::Point(83, 195);
			this->grayscale_chkbox->Name = L"grayscale_chkbox";
			this->grayscale_chkbox->Size = System::Drawing::Size(73, 17);
			this->grayscale_chkbox->TabIndex = 10;
			this->grayscale_chkbox->Text = L"Grayscale";
			this->grayscale_chkbox->UseVisualStyleBackColor = true;
			this->grayscale_chkbox->CheckedChanged += gcnew System::EventHandler(this, &MyForm::grayscale_chkbox_CheckedChanged);
			// 
			// sobel_chkbox
			// 
			this->sobel_chkbox->AutoSize = true;
			this->sobel_chkbox->Location = System::Drawing::Point(9, 195);
			this->sobel_chkbox->Name = L"sobel_chkbox";
			this->sobel_chkbox->Size = System::Drawing::Size(53, 17);
			this->sobel_chkbox->TabIndex = 9;
			this->sobel_chkbox->Text = L"Sobel";
			this->sobel_chkbox->UseVisualStyleBackColor = true;
			this->sobel_chkbox->CheckedChanged += gcnew System::EventHandler(this, &MyForm::sobel_chkbox_CheckedChanged);
			// 
			// canny_chkbox
			// 
			this->canny_chkbox->AutoSize = true;
			this->canny_chkbox->Location = System::Drawing::Point(9, 172);
			this->canny_chkbox->Name = L"canny_chkbox";
			this->canny_chkbox->Size = System::Drawing::Size(56, 17);
			this->canny_chkbox->TabIndex = 8;
			this->canny_chkbox->Text = L"Canny";
			this->canny_chkbox->UseVisualStyleBackColor = true;
			this->canny_chkbox->CheckedChanged += gcnew System::EventHandler(this, &MyForm::canny_chkbox_CheckedChanged);
			// 
			// resize_rotate_groupbox
			// 
			this->resize_rotate_groupbox->Controls->Add(this->resize_double_btn);
			this->resize_rotate_groupbox->Controls->Add(this->resize_half_btn);
			this->resize_rotate_groupbox->Location = System::Drawing::Point(12, 235);
			this->resize_rotate_groupbox->Name = L"resize_rotate_groupbox";
			this->resize_rotate_groupbox->Size = System::Drawing::Size(95, 94);
			this->resize_rotate_groupbox->TabIndex = 9;
			this->resize_rotate_groupbox->TabStop = false;
			this->resize_rotate_groupbox->Text = L"Resize video:";
			// 
			// resize_double_btn
			// 
			this->resize_double_btn->Location = System::Drawing::Point(10, 60);
			this->resize_double_btn->Name = L"resize_double_btn";
			this->resize_double_btn->Size = System::Drawing::Size(75, 23);
			this->resize_double_btn->TabIndex = 1;
			this->resize_double_btn->Text = L"Double Size";
			this->resize_double_btn->UseVisualStyleBackColor = true;
			this->resize_double_btn->Click += gcnew System::EventHandler(this, &MyForm::resize_double_btn_Click);
			// 
			// resize_half_btn
			// 
			this->resize_half_btn->Location = System::Drawing::Point(9, 25);
			this->resize_half_btn->Name = L"resize_half_btn";
			this->resize_half_btn->Size = System::Drawing::Size(75, 23);
			this->resize_half_btn->TabIndex = 0;
			this->resize_half_btn->Text = L"Half Size";
			this->resize_half_btn->UseVisualStyleBackColor = true;
			this->resize_half_btn->Click += gcnew System::EventHandler(this, &MyForm::resize_half_btn_Click);
			// 
			// rotate_left_btn
			// 
			this->rotate_left_btn->Location = System::Drawing::Point(9, 25);
			this->rotate_left_btn->Name = L"rotate_left_btn";
			this->rotate_left_btn->Size = System::Drawing::Size(75, 23);
			this->rotate_left_btn->TabIndex = 0;
			this->rotate_left_btn->Text = L"Rotate Left";
			this->rotate_left_btn->UseVisualStyleBackColor = true;
			this->rotate_left_btn->Click += gcnew System::EventHandler(this, &MyForm::rotate_left_btn_Click);
			// 
			// rotate_right_btn
			// 
			this->rotate_right_btn->Location = System::Drawing::Point(9, 60);
			this->rotate_right_btn->Name = L"rotate_right_btn";
			this->rotate_right_btn->Size = System::Drawing::Size(75, 23);
			this->rotate_right_btn->TabIndex = 1;
			this->rotate_right_btn->Text = L"Rotate Right";
			this->rotate_right_btn->UseVisualStyleBackColor = true;
			this->rotate_right_btn->Click += gcnew System::EventHandler(this, &MyForm::rotate_right_btn_Click);
			// 
			// record_stop_btn
			// 
			this->record_stop_btn->Enabled = false;
			this->record_stop_btn->Location = System::Drawing::Point(7, 60);
			this->record_stop_btn->Name = L"record_stop_btn";
			this->record_stop_btn->Size = System::Drawing::Size(75, 23);
			this->record_stop_btn->TabIndex = 1;
			this->record_stop_btn->Text = L"Stop Rec";
			this->record_stop_btn->UseVisualStyleBackColor = true;
			this->record_stop_btn->Click += gcnew System::EventHandler(this, &MyForm::record_stop_btn_Click);
			// 
			// record_start_btn
			// 
			this->record_start_btn->Location = System::Drawing::Point(7, 25);
			this->record_start_btn->Name = L"record_start_btn";
			this->record_start_btn->Size = System::Drawing::Size(75, 23);
			this->record_start_btn->TabIndex = 0;
			this->record_start_btn->Text = L"Start Rec";
			this->record_start_btn->UseVisualStyleBackColor = true;
			this->record_start_btn->Click += gcnew System::EventHandler(this, &MyForm::record_start_btn_Click);
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->rotate_left_btn);
			this->groupBox1->Controls->Add(this->rotate_right_btn);
			this->groupBox1->Location = System::Drawing::Point(111, 235);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(93, 94);
			this->groupBox1->TabIndex = 12;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Rotate video:";
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->record_stop_btn);
			this->groupBox2->Controls->Add(this->record_start_btn);
			this->groupBox2->Location = System::Drawing::Point(210, 235);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(91, 94);
			this->groupBox2->TabIndex = 10;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Record video:";
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(311, 334);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->resize_rotate_groupbox);
			this->Controls->Add(this->img_adj_groupbox);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->Name = L"MyForm";
			this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
			this->Text = L"AftaEffects - Control Panel";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->brightness_slider))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->contrast_slider))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->blur_slider))->EndInit();
			this->img_adj_groupbox->ResumeLayout(false);
			this->img_adj_groupbox->PerformLayout();
			this->resize_rotate_groupbox->ResumeLayout(false);
			this->groupBox1->ResumeLayout(false);
			this->groupBox2->ResumeLayout(false);
			this->ResumeLayout(false);

		}

	private: cv::VideoCapture *cap;
	private: FrameProcessor^ fp;
	private: cv::VideoWriter *out;

	private: System::Void frame_timer_Tick(System::Object^ s, System::EventArgs^ e)
	{
		cv::Mat frame;
		*cap >> frame;

		if (frame.empty())
		{
			frame_timer->Enabled = false;
			return;
		}

		imshow("AftaEffects - Original Video", frame);

		frame = fp->ProcessFrame(frame);
		imshow("AftaEffects - Processed Video", frame);

		if (fp->isRecording() && out->isOpened())
			*out << frame;
	}

	private: System::Void canny_chkbox_CheckedChanged(System::Object^ s, System::EventArgs^ e)
	{
		if (canny_chkbox->Checked)
		{
			fp->setCanny(true);
			sobel_chkbox->Checked = false;
			grayscale_chkbox->Checked = true;
			grayscale_chkbox->Enabled = false;
		}
		else
		{
			grayscale_chkbox->Enabled = true;
			fp->setCanny(false);
		}
	}

	private: System::Void sobel_chkbox_CheckedChanged(System::Object^ s, System::EventArgs^ e)
	{
		if (sobel_chkbox->Checked)
		{
			fp->setSobel(true);
			canny_chkbox->Checked = false;
			grayscale_chkbox->Checked = true;
			grayscale_chkbox->Enabled = false;
		}
		else
		{
			grayscale_chkbox->Enabled = true;
			fp->setSobel(false);
		}
	}

	private: System::Void brightness_slider_ValueChanged(System::Object^s, System::EventArgs^ e)
	{
		Int32 text_val;

		try
		{
			text_val = Convert::ToInt32(brightness_val->Text);
		}
		catch (...)
		{
			text_val = 0;
		}

		if (brightness_slider->Value != text_val)
			brightness_val->Text = Convert::ToString(brightness_slider->Value);

		fp->setBrightnessAmount(brightness_slider->Value);
	}

	private: System::Void contrast_slider_ValueChanged(System::Object^s, System::EventArgs^ e)
	{
		Int32 text_val;

		try
		{
			text_val = Convert::ToInt32(contrast_val->Text);
		}
		catch (...)
		{
			text_val = 0;
		}

		if (contrast_slider->Value != text_val)
			contrast_val->Text = Convert::ToString(contrast_slider->Value);

		fp->setContrastAmount(contrast_slider->Value);
	}

	private: System::Void blur_slider_ValueChanged(System::Object^ s, System::EventArgs^ e)
	{
		Int32 text_val;

		try
		{
			text_val = Convert::ToInt32(blur_val->Text);
		}
		catch (...)
		{
			text_val = 0;
		}

		if (blur_slider->Value != text_val)
			blur_val->Text = Convert::ToString(blur_slider->Value);

		fp->setFilterAmount(blur_slider->Value);
	}

	private: System::Void brightness_val_TextChanged(System::Object^ s, System::EventArgs^ e)
	{
		Int32 text_val;

		try
		{
			text_val = Convert::ToInt32(brightness_val->Text);
		}
		catch (...)
		{
			text_val = 0;
		}

		text_val = clamp(text_val, -255, 255);
		if (brightness_slider->Value != text_val)
			brightness_slider->Value = text_val;

		fp->setBrightnessAmount(brightness_slider->Value);
	}

	private: System::Void contrast_val_TextChanged(System::Object^ s, System::EventArgs^ e)
	{
		Int32 text_val;

		try
		{
			text_val = Convert::ToInt32(contrast_val->Text);
		}
		catch (...)
		{
			text_val = 0;
		}

		text_val = clamp(text_val, 1, 100);
		if (contrast_slider->Value != text_val)
			contrast_slider->Value = text_val;

		fp->setContrastAmount(contrast_slider->Value);
	}

	private: System::Void blur_val_TextChanged(System::Object^ s, System::EventArgs^ e)
	{
		Int32 text_val;

		try
		{
			text_val = Convert::ToInt32(blur_val->Text);
		}
		catch (...)
		{
			text_val = 0;
		}

		text_val = clamp(text_val, 0, 10);
		if (blur_slider->Value != text_val)
			blur_slider->Value = text_val;

		fp->setFilterAmount(blur_slider->Value);
	}

	private: System::Void grayscale_chkbox_CheckedChanged(System::Object^ s, System::EventArgs^ e)
	{
		fp->setGrayscale(grayscale_chkbox->Checked);
	}

	private: System::Void negative_chkbox_CheckedChanged(System::Object^ s, System::EventArgs^ e)
	{
		fp->setNegative(negative_chkbox->Checked);
	}

	private: System::Void h_mirror_chkbox_CheckedChanged(System::Object^ s, System::EventArgs^ e)
	{
		fp->setHorizontalMirror(h_mirror_chkbox->Checked);
	}

	private: System::Void v_mirror_chkbox_CheckedChanged(System::Object^ s, System::EventArgs^ e)
	{
		fp->setVerticalMirror(v_mirror_chkbox->Checked);
	}

	private: System::Void resize_half_btn_Click(System::Object^ s, System::EventArgs^ e)
	{
		fp->HalfSize();
	}

	private: System::Void resize_double_btn_Click(System::Object^ s, System::EventArgs^ e)
	{
		fp->DoubleSize();
	}

	private: System::Void rotate_left_btn_Click(System::Object^ s, System::EventArgs^ e)
	{
		fp->RotateLeft();
	}

	private: System::Void rotate_right_btn_Click(System::Object^ s, System::EventArgs^ e)
	{
		fp->RotateRight();
	}

	private: System::Void record_start_btn_Click(System::Object^ s, System::EventArgs^ e)
	{
		if (!cap->isOpened())
		{
			MessageBox::Show(L"Error:\nNo camera being captured!");
			return;
		}
		
		// ask for a filename
		SaveFileDialog dlg;
		dlg.Filter = "avi files (*.avi)|*.avi";
		if (dlg.ShowDialog() != System::Windows::Forms::DialogResult::OK)
			return;
		
		// open output file
		cv::String filename = marshal_as<std::string>(dlg.FileName);
		int width = static_cast<Int32>(cap->get(CV_CAP_PROP_FRAME_WIDTH));
		int height = static_cast<Int32>(cap->get(CV_CAP_PROP_FRAME_HEIGHT));
		out->open(filename, CV_FOURCC('M', 'J', 'P', 'G'), 10, cv::Size(width, height));

		if (!out->isOpened())
		{
			MessageBox::Show(L"Error:\nFile could not be opened!");
			return;
		}

		record_start_btn->Enabled = false;
		record_stop_btn->Enabled = true;
		resize_half_btn->Enabled = false;
		resize_double_btn->Enabled = false;
		rotate_left_btn->Enabled = false;
		rotate_right_btn->Enabled = false;

		fp->setRecording(true);
	}

	private: System::Void record_stop_btn_Click(System::Object^ s, System::EventArgs^ e)
	{
		fp->setRecording(false);
		out->release();

		record_stop_btn->Enabled = false;
		record_start_btn->Enabled = true;
		resize_half_btn->Enabled = true;
		resize_double_btn->Enabled = true;
		rotate_left_btn->Enabled = true;
		rotate_right_btn->Enabled = true;
	}
};
}
