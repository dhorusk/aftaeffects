#pragma once

#pragma unmanaged
#include <opencv2/opencv.hpp>
#pragma managed

ref class FrameProcessor
{
public:
	enum class FrameOrientation : System::Int32
	{
		NORMAL,
		ROT_90_R,
		ROT_180,
		ROT_90_L,
	};

	FrameProcessor();
	cv::Mat ProcessFrame(cv::Mat frame);
	FrameOrientation getOrientation();
	System::Boolean isRecording();
	System::Void setBrightnessAmount(System::Int32 val);
	System::Void setContrastAmount(System::Int32 val);
	System::Void setFilterAmount(System::Int32 val);
	System::Void setCanny(System::Boolean val);
	System::Void setSobel(System::Boolean val);
	System::Void setGrayscale(System::Boolean val);
	System::Void setNegative(System::Boolean val);
	System::Void setHorizontalMirror(System::Boolean val);
	System::Void setVerticalMirror(System::Boolean val);
	System::Void setRecording(System::Boolean val);
	System::Void setScaleFactor(System::Double val);
	System::Void setOrientation(FrameOrientation val);
	System::Void HalfSize();
	System::Void DoubleSize();
	System::Void RotateLeft();
	System::Void RotateRight();

private:
	System::Boolean _canny;
	System::Boolean _sobel;
	System::Boolean _grayscale;
	System::Boolean _negative;
	System::Boolean _h_mirror;
	System::Boolean _v_mirror;
	System::Boolean _recording;
	System::Double _scale_factor;
	System::Int32 _brightness_amount;
	System::Int32 _contrast_amount;
	System::Int32 _filter_amount;
	FrameOrientation _orientation;
};
